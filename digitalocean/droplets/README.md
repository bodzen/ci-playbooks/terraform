# DigitalOcean Droplet Creation

The following env variable must be set, to define which datacenter will receive the droplet:
```
TF_VAR_DO_DROPLET_REGION

e.g. TF_VAR_DO_DROPLET_REGION ~> NYC1
```

In the main gitlab-ci playbook, the name of the droplet can be define using the variable:
```
NEW_DROPLET_NAME
```
